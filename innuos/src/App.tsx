import React, {useState} from 'react';
import './App.css';
import {AlbumList} from "./components/album/AlbumList";

function App() {

  const [show,setShow] = useState<boolean>(false);
  return (
    <div className="App">
        {show ? <AlbumList/> : <header className="App-header">
            <button onClick={()=> setShow(true)} className="App-button">GET ALBUM</button>
        </header>}
    </div>
  );
}

export default App;
