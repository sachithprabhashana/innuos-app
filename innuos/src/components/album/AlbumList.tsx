import React, {useEffect, useState} from 'react';
import {Album} from "./Album";

export const AlbumList = () =>{
    const [data,setData]=useState<any[]>([]);
    const getData=()=>{
        fetch('albums.json'
            ,{
                headers : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
        )
            .then(function(response){
                console.log(response)
                return response.json();
            })
            .then(function(myJson) {
                console.log(myJson);
                setData(myJson)
            });
    }
    useEffect(()=>{
        getData()
    },[])

    return(



        <div className= "list-header">

            {data.map((item, index) => (

                <Album
                    key={index}
                    artist={item.artist}
                    album={item.album}
                    covers={item.cover ?? 'undefined_album_cover.png'}

                />

            ))}
        </div>

    );
};
