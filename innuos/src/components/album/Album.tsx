import React, {FC} from 'react';

type Props = {
source?: any;
covers?: any;
album: string;
artist: string;
};
export const Album : FC<Props> = ({source,covers,album,artist}) =>{
    return(

        <div className="app-container">
        <div className="image-logo-container">
            <img className="image" src={covers}/>
            <img className="logo"  src={require('../../assets/images/qobuz.png')}/>
            <div className="title-container">
                <h1 className="title1">{album}</h1>
                <p className="description1">{artist}</p>
            </div>


        </div>
        </div>
    )
}
